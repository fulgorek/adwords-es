# Docker adwords test with Elasticsearch and Kibana

This is an unofficial, open-source and community-driven boilerplate for ES based projects that run on Docker-Compose. It's an attempt of standardizing and making it easier to bootstrap LAMP applications ready for development environments.
The main services included are:

* Elasticsearch 6.3
* Kibana 6.3.

## Requirements

* Install Docker > 1.12
* Install Docker Compose > 1.11

Before anything, you need to make sure you have Docker properly setup in your environment. For that, refer to the official documentation for both Docker and Docker Compose.

Also, if you're developing on Mac or Windows – yeah, maybe that's the case –, make sure you have Docker Machine properly setup.

## Usage

You are up and running in two simple step:
```sh
$ docker-compose up --build -d
$ ./pipeline.sh
open http://127.0.0.1:5601/app/kibana#/discover
```

Enjoy!