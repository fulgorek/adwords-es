#!/usr/bin/env bash
WORKDIR="$( cd "$(dirname "$0")"; pwd -P)"

function _test(){
  type ${1} &> /dev/null
  [[ $? -ne 0 ]] && echo "${1} not installed... aborting" && exit 1
}

_test node
_test npm

cd ${WORKDIR}/pipeline
npm install &>/dev/null
HOST=127.0.0.1 npm start
